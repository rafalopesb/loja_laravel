@extends('loja_template')
@section('content')
<div>
    <h1>Produtos</h1>
    <div class="row mt-3 mb-4 text-center">
        <?php foreach($produtos as $produto){  ?>

        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 form-group">
            <div class="product border">
                <a href="/loja/produtos/<?=$produto->id?>">
                    <div class="product-img">
                        <img src='images/Produtos/{{$produto->imagem ?? "no-image.png"}}' alt="{{$produto->descricao}}" width="140px")); ?>
                    </div>

                    <div class="product-block">
                        <h5 class="truncate">
                            <?= $produto->descricao; ?>
                        </h5>
                        <h5 class="text-danger">
                            <?= 'R$ ' . number_format(floatval($produto->preco), 2, ',', '.');?>
                        </h5>
                        <ul class="list-inline ">
                            <li class="list-inline-item">
                                <i class="fa fa-credit-card"></i> 12x de
                                <?= 'R$ ' . number_format(floatval($produto->preco / 12), 2, ',', '.');?>
                            </li>
                        </ul>
                    </div>
                </a>
                <div class="product-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($produto->frete_gratis) {?>
                            <i class="fa fa-truck" aria-hidden="true"></i>
                            <b style="color: green">Frete Grátis</b>
                            <?php } else {?>
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <b style="color: orange">10% off</b>
                            <?php }?>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-outline-secondary btn-sm">Contact Seller</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       <?php } ?>
    </div>

    <hr>
    
     <?php // $this->element('categories_widget', array('categorias'=> $categorias, 'title'=> 'Categorias')) ?>

    <ul class="list-group" style="margin: 0px !important">
        <li class="list-group-item" style="margin-right: 0px !important; margin-left: 0px !important">{{url('/produtos')}}</li>
        <li class="list-group-item" style="margin-right: 0px !important; margin-left: 0px !important">{{url('/categorias')}}</li>
    </ul>
    
</div>
@endsection