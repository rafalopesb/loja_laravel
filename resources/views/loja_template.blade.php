<html>
    <head>
        <title>App Name - @yield('title')</title>
        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/layout_loja.css">
        <link rel="stylesheet" href="/libs/autocomplete/auto-complete.css">
        <meta name="csrf_token" content="{{csrf_token()}}">
    </head>
    <body>
        @section('header')
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header"> 
                    <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-8" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">
                    <img src="/images/tray-novamarca.png" width="140px" alt="Tray">                    </a> 
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/loja/dashboard">Home</a></li>
                </ul>
                <div class="pull-right">
                        <div style="
                            display: inline;
                            position:  relative;
                            float: left;
                        ">
                            <input class="form-control" id="autocomplete" style="
                            display: inline;
                            top: 7px;
                            position:  relative;
                        ">
                        </div>
                    <div class="dropdown" style="display: inline;">
                        <ul class="nav navbar-nav">
                        <li>
                            <a href="/loja/carrinho/checkout"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">rafalopesb<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Configurações</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/loja/users/logout">Logout</a></li>
                            </ul>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        @show

        <div class="container">
            @yield('content')
        </div>

        
        <script rel="text/javascript" src="/libs/autocomplete/auto-complete.js"></script>
        <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
        <script>

        $(document).ready(function(){
            new autoComplete({
                selector: '#autocomplete',
                minChars: 1,
                cache: false,
                renderItem: function (item, search){
                    
                    var imagem = $.type(item._source.imagem) == "string" &&  item._source.imagem.length > 0 ?
                        item._source.imagem : 'no-image.png';

                    return '<div class="autocomplete-suggestion" data-val="' + item._source.nome + '">\
                        <img  style="width: 40px" src="/images/Produtos/'+imagem+'"> ' + item.highlight.nome + '</div>';
                },
                source: function(term, response){
                    $.ajax({
                        url: "http://localhost:8003/api/search",
                        data: {q: $("#autocomplete").val()},
                        dataType: "json",
                        success: function(data){
                            
                            var dados = [];
                            for( i =0; i < data.dados.length; i++){
                            // for( i =0; i < data.dados[0].options.length; i++){

                                dados.push(data.dados[i]);
                                // dados.push(data.dados[0].options[i]);
                            }
                            
                            response(dados);
                        }
                    })
                }
            });
        })
            
        </script>
    </body>
</html>