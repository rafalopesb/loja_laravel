@extends('loja_template')
@section('content')
<h1>Nova Loja</h1>

@if ($errors->any())
 <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{url('/cadastros/loja')}}{{isset($loja->id) ? '/atualizar/'.$loja->id: '/salvar'}}" method="POST">
    @method('POST')

    <div class="form-group">
        <span>Nome</span>
        <input type="text" class="form-control" name="nome" value="{{ $loja->nome ?? ''}}">
    </div>

    <div class="form-group">
        <span>descricao</span>
        <input type="text" class="form-control" name="descricao" value="{{ $loja->descricao ?? '' }}">
    </div>

    <div class="form-group">
        <span>Logo</span>
        <input type="text" class="form-control" name="logo" value="{{ $loja->logo ?? ''}}">
    </div>

    <div class="form-group">
        <span>slug</span>
        <input type="text" class="form-control" name="slug" value="{{ $loja->slug ?? ''}}">
    </div>

    @csrf
    <button type="submit" class="btn btn-primary">Salvar</button>
    <a class="btn btn-warning" href="{{url('cadastros/loja')}}">Cancelar</a>
</form>
@endsection