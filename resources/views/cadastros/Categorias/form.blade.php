@extends('loja_template')
@section('content')
<h1>Nova Categoria</h1>

@if ($errors->any())
 <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{url('/cadastros/categoria')}}{{isset($categoria->id) ? '/atualizar/'.$categoria->id: '/salvar'}}" method="POST">
    @method('POST')

    <div class="form-group">
        <span>Descricao</span>
        <input type="text" class="form-control" name="descricao" value="{{ $categoria->descricao ?? '' }}">
    </div>

    <input type="hidden" name="store_id" value="1">
    @csrf
    <button type="submit" class="btn btn-primary">Salvar</button>
    <a class="btn btn-warning" href="{{url('cadastros/categoria')}}">Cancelar</a>
</form>
@endsection