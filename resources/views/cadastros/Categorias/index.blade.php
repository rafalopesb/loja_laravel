@extends('loja_template')
@section('content')
<h1>Cadastro de Categoria</h1>

@if (isset($message) && strlen($message) > 0)
<div class="alert alert-success">
    {{ $message }}
</div>
@endif
<div>
    <a href="{{ url('cadastros/categoria/novo') }}" class="btn btn-primary">Novo</a>
</div>
<table class="table table-striped table-hover table-reflow">
    <thead>
        <tr>
            <th ><strong> ID </strong></th>
            <th ><strong> Descricao </strong></th>
            <th ></th>
            <th ></th>
        </tr>
    </thead>
    <tbody>
        @foreach($categorias as $key=>$value)
                <tr>
                    <td>{{ $value->id }} </td>
                    <td>{{ $value->descricao }} </td>
                    <td><a class="btn btn-primary" href="{{url('cadastros/categoria/editar/'.$value->id)}}">Editar</a></td>
                    <td><a class="btn btn-danger" href="{{url('cadastros/categoria/deletar/'.$value->id)}}">Deletar</a></td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection