@extends('loja_template')
@section('content')
<h1>Novo Produto</h1>

@if ($errors->any())
 <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{url('/cadastros/produto')}}{{isset($produto->id) ? '/atualizar/'.$produto->id: '/salvar'}}" method="POST">
    @method('POST')

    <div class="form-group">
        <span>Nome</span>
        <input type="text" class="form-control" name="descricao" value="{{ $produto->descricao ?? ''}}">
    </div>

    <div class="form-group">
        <span>Preço</span>
        <input type="text" class="form-control" name="preco" value="{{ $produto->preco ?? '' }}">
    </div>

    <div class="form-group">
        <span>Categoria</span>
        <input type="text" class="form-control" name="categoria_id" value="{{ $produto->categoria_id ?? ''}}">
    </div>

    <input type="hidden" name="store_id" value="1">
    @csrf
    <button type="submit" class="btn btn-primary">Salvar</button>
    <a class="btn btn-warning" href="{{url('cadastros/produto')}}">Cancelar</a>
</form>
@endsection