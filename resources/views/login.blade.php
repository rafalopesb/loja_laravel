<!doctype html>
<html>
<head>
<title>Look at me Login</title>

<link rel="stylesheet" href="css/bootstrap.css">
<style>
.col-centered{
    float: none;
    margin: 0 auto;
}

.v-center{
   display: flex;
   align-items: center;
}
</style>
</head>
<body>

<form id="login" method="post" action="/login">

<div class="col-md-12">
    
    <div class="col-md-6 col-centered ">
        <h1>Login</h1>

        <!-- if there are login errors, show them here -->
        <p>
            @if($errors_msg)
            <div class="alert alert-danger">
                {{ $errors_msg }}
            </div>
            @endif
        </p>

        <p>
            <label for="email">Email</label>
            
            <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="usuario@tray.net.br">
        </p>

        <p>
            <label for="password">Password</label>
            <input type="password" id="password" value="{{ old('password') }}" name="password" class="form-control">
        </p>

        {{ csrf_field() }}
        <p><button class="btn btn-primary">Login</button></p>
        </form>
    </div>
</div>