<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loja extends Model
{
    public $fillable = ['nome', 'descricao', 'logo', 'slug', 'user_id'];
    public function scopeWhereUser($query)
    {
        
        return $query->where('user_id', \Request::user()->id);
    }
}
