<?php

namespace App\Listeners;

use App\Events\IndexadorElastic;
use App\Services\SearchService;

class IndexaProdutoElastic
{
    private $SearchService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SearchService $SearchService)
    {
        //
        $this->SearchService = $SearchService;
    }

    /**
     * Handle the event.
     *
     * @param  IndexadorElastic  $event
     * @return void
     */
    public function handle(IndexadorElastic $event)
    {
        //
        if ($event->tipo == "insert") {

            $retorno = $this->SearchService->createIndexProducts($event->dados['products'], 2, $event->ESClient);
        }

        if ($event->tipo == "delete") {
            $retorno = $this->SearchService->removeIndexProduct($event->dados['id'], 2, $event->ESClient);
        }

        if ($event->tipo == "update") {
            $retorno = $this->SearchService->updateIndexProduct($event->dados['product'], 2, $event->ESClient);
            var_dump($retorno);
        }
    }
}
