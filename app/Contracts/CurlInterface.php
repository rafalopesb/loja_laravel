<?php

namespace App\Contracts;

interface CurlInterface{

    public function setConfig(Array $parametros);

    public function getCurl();
}
?>