<?php

namespace App;

use App\Services\CurlGet;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['descricao', 'preco', 'frete_gratis', 'categoria_id', 'imagem', 'store_id'];

    public function categoria(){
        
        return $this->belongsTo('App\Categoria');
    }

   
}
