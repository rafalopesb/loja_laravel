<?php

namespace App\Events;

use App\Traits\ESTrait;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IndexadorElastic
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ESTrait;

    public $tipo;
    public $dados;
    public $ESClient;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($tipo, $dados)
    {
        //
        $this->tipo = $tipo;
        $this->dados = $dados;
        $this->ESClient = $this->getElasticClient();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

        return new PrivateChannel('channel-name');
    }
}
