<?php

namespace App\Services;

use App\Contracts\CurlInterface;
use App\Traits\CurlRequestsTrait;

class CurlDelete implements CurlInterface
{

    use CurlRequestsTrait;

    private $curl;

    public function __construct(String $url)
    {
        $this->curl = curl_init($url);
    }

    public function setConfig($parametros)
    {
        $this->setDefaultConfig($this->curl);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    }

    public function getCurl()
    {
        return $this->curl;
    }
}
