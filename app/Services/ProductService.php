<?php

namespace App\Services;

use App\Produto;

class ProductService
{

    private $Produto;

    public function __construct(Produto $produto){
        $this->Produto = $produto;
    }

    public function getProductsByStoreId($id)
    {
        return $this->Produto->where('store_id', $id)->get();
    }
}
