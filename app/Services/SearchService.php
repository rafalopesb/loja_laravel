<?php

namespace App\Services;

class SearchService
{

    public function getSearchService()
    {
        return $this;
    }

    public function createIndexProducts($produtos, $store_id, $ESClient)
    {
        $responses = [];
        foreach ($produtos as $prd) {

            $params = [
                'index' => 'store_' . $store_id,
                'type' => 'my_type',
                'id' => $prd['id'],
                'body' => ['nome' => $prd['descricao'], 'id' => $prd['id'], 'imagem' => $prd['imagem']]
            ];

            $responses[] = $ESClient->index($params);
        }

        return $responses;
    }

    public function removeIndexProduct($id, $store_id, $ESClient)
    {
        return $ESClient->delete([
            'index' => 'store_' . $store_id,
            'type' => 'my_type',
            'id' => $id,
        ]);

    }

    public function updateIndexProduct($produto, $store_id, $ESClient){
      
        return $ESClient->update([
            'index' => 'store_' . $store_id,
            'type' => 'my_type',
            'id' => $produto['id'],
            'body' => [
                'doc' => [
                    'nome' => $produto['descricao']
                ]
            ]
        ]);
    }
}
