<?php

namespace App\Services;

use App\Contracts\CurlInterface;

class CurlRequestsService {

    private $headers = [];
    private $ch;
    public const urlApi = "localhost:8003/api/";

    public function createConfigRequest(CurlInterface $curlObj, $dados = []) {
        
        $curlObj->setConfig($dados);
        $this->ch = $curlObj;
    }

    public function execute(){
        
        $content = trim(curl_exec($this->ch->getCurl()));
        $httpcode = curl_getinfo($this->ch->getCurl(), CURLINFO_HTTP_CODE);
        
        return array('status'=> $httpcode, 'content'=> $content);
    }
}
?>  