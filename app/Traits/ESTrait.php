<?php
namespace App\Traits;

use Illuminate\Support\Facades\App;

trait ESTrait
{
    /**
     *
     * @return \Elasticsearch\Client
     */
    public function getElasticClient()
    {

        return App::call([$this, 'getElasticClientInstance'])->build();
    }

    /**
     * Get elastic search index name for the logged and routed user
     *
     * @param int $sellerId
     * @return string
     */
    public function getIndexName(int $sellerId = 0)
    {
        return App::call([$this, 'getSellerServiceInstance'])->getIndex($sellerId);
    }

    /**
     *
     * @param \App\Services\SellerService $sellerService
     * @return \App\Services\SellerService
     */
    public function getSellerServiceInstance(\App\Services\SellerService $sellerService)
    {
        return $sellerService;
    }

    /**
     *
     * @param \Elasticsearch\Client $esClient
     * @return \Elasticsearch\Client
     */
    public function getElasticClientInstance(\Elasticsearch\ClientBuilder $esClient)
    {
        return $esClient;
    }
}
