<?php

namespace App\Traits;

trait CurlRequestsTrait
{

    public function setDefaultConfig($curl){        
        
        $headers = ['X-Requested-With:XMLHttpRequest'];
        
        if(session('user_token')){
            $headers[] = 'Authorization:Bearer '.session('user_token')->access_token;
        }
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    }

    public function getCurl(){
        return $this->curl;
    }
}
