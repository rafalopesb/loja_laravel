<?php

namespace App\Repositories;

use App\Services\CurlDelete;
use App\Services\CurlGet;
use App\Services\CurlPost;
use App\Services\CurlPut;
use App\Services\CurlRequestsService;

class RequestRepository
{
    private $endpoint;
    public function __construct(CurlRequestsService $curlService)
    {
        $this->curlService = $curlService;
    }

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function getFromApi($id)
    {
        $this->curlService->createConfigRequest(new CurlGet($this->curlService::urlApi . "$this->endpoint/" . $id));
        $retornoCurl = $this->curlService->execute();

        return json_decode($retornoCurl['content']);
    }

    public function getAllFromApi($parametros = false)
    {
        $this->curlService->createConfigRequest(new CurlGet($this->curlService::urlApi . "$this->endpoint"),
            $parametros);
        $retornoCurl = $this->curlService->execute();
        
        return json_decode($retornoCurl['content']);
    }

    public function save($parametros)
    {
        $this->curlService->createConfigRequest(new CurlPost($this->curlService::urlApi . "$this->endpoint"),
            $parametros);

        return $this->curlService->execute();
    }

    public function update($parametros, $id)
    {
        $this->curlService->createConfigRequest(new CurlPut($this->curlService::urlApi . "$this->endpoint/" . $id),
            $parametros);

        return $this->curlService->execute();
    }

    public function delete($id)
    {

        $this->curlService->createConfigRequest(new CurlDelete($this->curlService::urlApi . "$this->endpoint/" . $id));

        return $this->curlService->execute();
    }
}
