<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $store_id = $request->storeid;
        if(DB::table('lojas')->where('slug', $store_id)->exists()){
            return $next($request);
        }
        
        abort(404);
    }
}
