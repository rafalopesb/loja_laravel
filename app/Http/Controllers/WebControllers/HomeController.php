<?php

namespace App\Http\Controllers\WebControllers;

use App\Http\Controllers\Controller;
use App\Repositories\RequestRepository;

class HomeController extends Controller
{
    protected $requestRepository;

    public function __construct(RequestRepository $repository)
    {
        $this->requestRepository = $repository;
    }

    public function index()
    {
        $this->requestRepository->setEndpoint('product');
        $produtos = $this->requestRepository->getAllFromApi(['store_id' => 1])->produtos;

        $this->requestRepository->setEndpoint('product');
        $produtos = $this->requestRepository->getAllFromApi()->produtos;

        return view('Home/index')->with('produtos', $produtos);
    }
}
