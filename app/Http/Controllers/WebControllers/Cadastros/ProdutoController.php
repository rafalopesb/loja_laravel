<?php

namespace App\Http\Controllers\WebControllers\cadastros;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoRequest;
use App\Repositories\RequestRepository;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{

    protected $curlService;
    protected $produtoRepository;

    public function __construct(RequestRepository $repository)
    {
        $repository->setEndpoint('product');
        $this->produtoRepository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastros/Produtos/index')
            ->with('produtos', $this->produtoRepository->getAllFromApi()->produtos)
            ->with('message', \Session::get('message') ?? '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function novo()
    {
        return view('cadastros/Produtos/form')->with('produto', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function salvar(ProdutoRequest $request)
    {
        $retornoCurl = $this->produtoRepository->save(
            $request->only(['descricao', 'preco', 'categoria_id', 'store_id'])
        );

        $errors = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao salvar o produto";
            return redirect('cadastros/produto/novo')->withErrors($errors);
        }

        return redirect('cadastros/produto')->with('message', 'Produto cadastrado');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        return view('cadastros/Produtos/form')
            ->with('produto', $this->produtoRepository->getFromApi($id)->produto)
            ->with('message', \Session::get('message') ?? '');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atualizar(ProdutoRequest $request, $id)
    {

        $retornoCurl = $this->produtoRepository->update($request->only(['descricao', 'preco', 'categoria_id']), $id);

        $errors = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao editar o produto, tente novamente";
            return redirect('cadastros/produto/editar/' . $id)->withErrors($errors);
        }

        return redirect('cadastros/produto')->with('message', 'Produto Editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletar($id)
    {
        $retornoCurl = $this->produtoRepository->delete($id);

        $errors = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao deletar o produto, tente novamente";
            return redirect('cadastros/produto')->withErrors($errors);
        }

        return redirect('cadastros/produto')->with('message', 'Produto deletado');
    }
}
