<?php

namespace App\Http\Controllers\WebControllers\cadastros;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaRequest;
use App\Repositories\RequestRepository;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    protected $curlService;
    protected $categoriaRepository;

    public function __construct(RequestRepository $repository)
    {
        $repository->setEndpoint('category');
        $this->categoriaRepository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastros/Categorias/index')
            ->with('categorias', $this->categoriaRepository->getAllFromApi()->categorias)
            ->with('message', \Session::get('message') ?? '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function novo()
    {
        return view('cadastros/Categorias/form')->with('categorias', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function salvar(CategoriaRequest $request)
    {
        $retornoCurl = $this->categoriaRepository->save(
            $request->only(['descricao', 'store_id'])
        );
        
        $errors = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao salvar a categoria";
            return redirect('cadastros/categoria/novo')->withErrors($errors);
        }

        return redirect('cadastros/categoria')->with('message', 'Categoria cadastrada');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        return view('cadastros/Categorias/form')
            ->with('categoria', $this->categoriaRepository->getFromApi($id)->categoria)
            ->with('message', \Session::get('message') ?? '');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atualizar(CategoriaRequest $request, $id)
    {

        $retornoCurl = $this->categoriaRepository->update($request->only(['descricao', 'store_id']), $id);

        $errors = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao editar a categoria, tente novamente";
            return redirect('cadastros/categoria/editar/' . $id)->withErrors($errors);
        }

        return redirect('cadastros/categoria')->with('message', 'Categoria Editada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletar($id)
    {
        $retornoCurl = $this->categoriaRepository->delete($id);

        $errors = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao deletar a categoria, tente novamente";
            return redirect('cadastros/categoria')->withErrors($errors);
        }

        return redirect('cadastros/categoria')->with('message', 'Categoria deletada');
    }
}
