<?php

namespace App\Http\Controllers\WebControllers\cadastros;

use App\Http\Controllers\Controller;
use App\Http\Requests\LojaRequest;
use App\Repositories\RequestRepository;
use Illuminate\Http\Request;

class LojaController extends Controller
{

    protected $curlService;
    protected $lojaRepository;

    public function __construct(RequestRepository $repository)
    {
        $repository->setEndpoint('store');
        $this->lojaRepository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastros/Lojas/index')
            ->with('lojas', $this->lojaRepository->getAllFromApi()->lojas)
            ->with('message', \Session::get('message') ?? '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function novo()
    {
        return view('cadastros/Lojas/form')->with('lojas', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function salvar(LojaRequest $request)
    {
        $retornoCurl = $this->lojaRepository->save(
            $request->only(['nome', 'descricao', 'logo', 'slug'])
        );

        $errors  = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao salvar a loja";
            return redirect('cadastros/loja/novo')->withErrors($errors);
        }
        
        return redirect('cadastros/loja')->with('message', 'Loja cadastrada');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        return view('cadastros/Lojas/form')
            ->with('loja', $this->lojaRepository->getFromApi($id)->loja)
            ->with('message', \Session::get('message') ?? '');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atualizar(LojaRequest $request, $id)
    {

        $retornoCurl = $this->lojaRepository->update($request->only(['nome', 'descricao', 'logo', 'slug']), $id);
        
        $errors  = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao editar a loja, tente novamente";
            return redirect('cadastros/loja/editar/'.$id)->withErrors($errors);
        }

        return redirect('cadastros/loja')->with('message', 'Loja Editada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletar($id)
    {
        $retornoCurl = $this->lojaRepository->delete($id);
        
        $errors  = [];
        if ($retornoCurl['status'] != 200) {
            $errors[] = "Ocorreu um erro ao deletar a loja, tente novamente";
            return redirect('cadastros/loja')->withErrors($errors);
        }

        return redirect('cadastros/loja')->with('message', 'Loja deletada');
    }
}
