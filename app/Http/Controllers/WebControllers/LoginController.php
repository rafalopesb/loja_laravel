<?php

namespace App\Http\Controllers\WebControllers;

use App\Services\CurlPost;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use App\Services\CurlRequestsService;

class LoginController extends Controller
{
    protected $curlService;

    public function __construct(CurlRequestsService $curlService){
        $this->curlService = $curlService;
    }

    public function index()
    {
        return view('login')->with('errors_msg', false);
    }

    public function login(LoginRequest $request)
    {
        
        $this->curlService->createConfigRequest(new CurlPost($this->urlApi."auth/login"), ['email'=> $request['email'], 'password'=> $request['password']]);
        $retornoCurl = $this->curlService->execute();
        
        if ($retornoCurl['status'] == 200 && strlen(json_decode($retornoCurl['content'])->access_token) > 0) {
            session(['user_token'=> json_decode($retornoCurl['content'])]);
            return redirect('dashboard');
        }
        
        $request->flash();
        return view('login')->with(array('errors_msg'=> 'usuario ou senha inválidos'));
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('login');
    }
}
