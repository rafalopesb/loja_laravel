<?php

namespace App\Http\Controllers\ApiControllers;

use App\Events\IndexadorElastic;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoRequest;
use App\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json(['status' => true, 'produtos' => Produto::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoRequest $produto)
    {
        $novo_produto = new Produto(
            $produto->only(array('descricao', 'preco', 'frete_gratis', 'categoria_id', 'store_id'))
        );

        if ($novo_produto->save()) {

            event(new IndexadorElastic('insert', array(
                'products' => [
                    [
                        'descricao' => $novo_produto->descricao,
                        'id' => $novo_produto->id,
                        'imagem' => ""
                    ],
                ],
            )));

            return response()->json(['status' => true, 'msg' => "Product added", "id" => $novo_produto->id]);
        }

        return response()->json(['status' => false, 'msg' => "An error ocurred - Product cannot be added"], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Produto::find($id);

        if (!is_null($produto)) {
            return response()->json(['status' => true, 'produto' => $produto]);
        }

        return response()->json(['status' => false, 'msg' => "Product not found"], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoRequest $request, $id)
    {
        $status = false;
        $produto = Produto::find($id);

        if (!is_null($produto)) {
            $fields = ['descricao', 'preco', 'categoria_id', 'frete_gratis'];
            $status = $produto->fill($request->only($fields))->save();

            if ($status) {
                
                event(new IndexadorElastic('update', 
                    [
                        'product'=> [
                            'descricao' => $request->input('descricao'),
                            'id' => $id,
                            'imagem' => ""
                        ]
                    ]
                ));

                return response()->json(['status' => true, 'msg' => "Product Edited"]);
            }
        }

        return response()->json(['status' => false, 'msg' => "An error ocurred - Product cannot be updated"], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);

        if (!is_null($produto)) {
            if ($produto->delete()) {
                event(new IndexadorElastic('delete', ['id' => $id]));
                return response()->json(['status' => true, 'msg' => "Product deleted"]);
            }

            return response()->json(['status' => false, 'msg' => "An error ocurred - Product cannot be deleted"], 404);
        }

        return response()->json(['status' => false, 'msg' => "Product not found"], 404);
    }
}
