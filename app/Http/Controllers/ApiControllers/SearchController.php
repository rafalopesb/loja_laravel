<?php

namespace App\Http\Controllers\ApiControllers;

use App\Traits\ESTrait;
use Illuminate\Http\Request;
use App\Services\SearchService;
use App\Services\ProductService;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    use ESTrait;

    private $Search;
    private $ESClient;

    public function __construct(SearchService $service){
        $this->Search = $service->getSearchService();
        $this->ESClient = $this->getElasticClient();
    }

    public function index(Request $request)
    {
        // $dados = $this->ESClient->search(array(
        //     'index' => 'store_1',
        //     'body'=> '{
        //         "suggest": {
        //           "product-suggest": {
        //             "prefix": "'.$request->input('q').'",
        //             "completion": {
        //               "field": "nome",
        //               "fuzzy" : {
        //                 "fuzziness" : 1
        //                }
        //             }
        //           }
        //         }
        //       }'
        // ));
        // $dados['hits']['hits'] = $dados['suggest']['product-suggest'];
        
        $dados = $this->ESClient->search(array(
            'index' => 'store_2',
            'body'=> [
                "query"=> [
                    "match"=> [
                        "nome"=> [
                            "query"=> $request->input('q'), 
                            "operator"=> "and"
                        ]
                    ]
                ],
                'highlight' => [
                    'fields' => [
                        'nome' => new \stdClass()
                    ],
                    'require_field_match' => false
                ]
            ]
        ));
        
        // return response()->json(['dados'=> $dados['suggest']['product-suggest']]);
        return response()->json(['dados'=> $dados['hits']['hits']]);
    }

    public function createIndexStore($id)
    {
        // $params = [
        //     'index' => 'store_' . $id,
        //     'body' => [
        //         'settings' => [
        //             'number_of_shards' => 3,
        //             'number_of_replicas' => 2,
        //         ],
        //         'mappings' => [
        //             'my_type' => [
        //                 'properties' => [
        //                     'nome' => [
        //                         'type' => 'completion',
        //                         'analyzer' => 'portuguese',
        //                     ],
        //                     'id' => [
        //                         'type' => 'integer'
        //                     ],
        //                     'imagem' => [
        //                         'type'=> 'text'
        //                     ]
        //                 ],
        //             ],
        //         ],
        //     ],
        // ];

        $params = [
            'index' => 'store_' . $id,
            'body' => [
                'settings' => [
                    'number_of_shards' => 3,
                    'number_of_replicas' => 2,
                    'analysis'=> [
                        'analyzer'=> [
                            'autocomplete'=> [
                                'tokenizer'=> 'autocomplete',
                                'filter'=> ['lowercase']
                            ],
                            'autocomplete_search'=> [
                                'tokenizer'=> 'lowercase'
                            ]
                        ],
                        'tokenizer'=> [
                            'autocomplete'=> [
                                'type'=> 'edge_ngram',
                                "min_gram"=> 1,
                                "max_gram"=> 10,
                                "token_chars"=> [
                                    "letter",
                                    "digit"
                                ]
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'my_type' => [
                        'properties' => [
                            'nome' => [
                                'type' => 'text',
                                'analyzer' => 'autocomplete',
                                "search_analyzer"=> "autocomplete_search"
                            ],
                            'id' => [
                                'type' => 'integer'
                            ],
                            'imagem' => [
                                'type'=> 'text'
                            ]
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->ESClient->indices()->create($params);

        var_dump($response);
    }

    public function insertProductsInES(Request $request, ProductService $Produto)
    {
        $id_loja = $request->input('store_id');
        $produtos = $Produto->getProductsByStoreId($id_loja);
        
        if(count($produtos) > 0){
            $this->Search->createIndexProducts($produtos->toArray(), $id_loja, $this->ESClient);
        } 
        
    }
}