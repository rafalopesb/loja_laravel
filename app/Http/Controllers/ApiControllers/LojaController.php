<?php

namespace App\Http\Controllers\ApiControllers;

use App\Loja;
use Illuminate\Http\Request;
use App\Http\Requests\LojaRequest;
use App\Http\Controllers\Controller;

class LojaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $lojas = Loja::whereUser()->get();

        if(!is_null($lojas)){
            return response()->json(['status'=> true, 'lojas'=> $lojas]);
        }

        return response()->json(['status'=> false, 'msg'=> "Store not found"], 404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LojaRequest $request)
    {
        $loja = new Loja(
            array_merge(
                $request->only(['nome', 'descricao', 'logo', 'slug']),
                ['user_id'=> \Request::user()->id]
            )
        );

        if($loja->save()){
            return response()->json(['status'=> true, 'msg'=> "Store added", "id"=> $loja->id]);
        }

        return response()->json(['status'=> false, 'msg'=> "An error ocurred - Store cannot be added"], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loja = Loja::whereUser()->find($id);
        
        if(is_null($loja)){
            return response()->json(['status'=> false, 'msg'=> "Store not found"], 404);
        }

        return response()->json(['status'=> true, 'loja'=> $loja]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LojaRequest $request, $id)
    {
        $status_update = Loja::whereUser()->find($id)->update($request->only(['nome', 'descricao', 'logo', 'slug']));

        if($status_update){
            return response()->json(['status'=> true, 'msg'=> "Store updated"]);
        }

        return response()->json(['status'=> false, 'msg'=> "An error ocurred - Store cannot be updated"], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $loja = Loja::whereUser()->find($id);

        if(!is_null($loja)){
            if($loja->delete()){
                return response()->json(['status'=> true, 'msg'=> "Store deleted"]);
            }
            
            return response()->json(['status'=> false, 'msg'=> "An error ocurred - Store cannot be deleted"], 404);
        }

        return response()->json(['status'=> false, 'msg'=> "Store not found"], 404);
    }
}
