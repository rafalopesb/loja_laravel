<?php

namespace App\Http\Controllers\ApiControllers;

use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaRequest;

class CategoriaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['status'=> true, 'categorias'=> Categoria::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $request)
    {
        $nova_categoria = new Categoria($request->only(['descricao', 'store_id']));

        if($nova_categoria->save()){
            return response()->json(['status'=> true, 'msg'=> "Category added", "id"=> $nova_categoria->id]);
        }

        return response()->json(['status'=> false, 'msg'=> "An error ocurred - Category cannot be added"], 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = Categoria::find($id);

        if(!is_null($categoria)){
            return response()->json(['status'=> true, 'categoria'=> $categoria]);
        }

        return response()->json(['status'=> false, 'msg'=> "Category not found"], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaRequest $request, $id)
    {
        $status = false;
        $categoria = Categoria::find($id);
        
        if(!is_null($categoria)){
            $status = $categoria->fill($request->only(array('descricao')))->save();

            if($status){
                return response()->json(['status'=> true, 'msg'=> "Category Edited"]);
            }
        }

        return response()->json(['status'=> false, 'msg'=> "An error ocurred - Category cannot be updated"], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_null($categoria = Categoria::find($id))){
            if($categoria->delete()){
                return response()->json(['status'=> true, 'msg'=> "Category deleted"]);
            }

            return response()->json(['status'=> false, 'msg'=> "An error ocurred - Category cannot be deleted"], 404);
        }

        return response()->json(['status'=> false, 'msg'=> "Category not found"], 404);
    }
}
