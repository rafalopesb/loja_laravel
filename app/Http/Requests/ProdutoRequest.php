<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            switch($this->method())
            {
                case 'POST':
                {
                    return [
                        'descricao' => 'required|string|max:255',
                        'preco'=> 'required|numeric',
                        // 'imagem'=> '',
                        'frete_gratis'=> 'boolean',
                        'categoria_id'=> 'required|numeric',
                        'store_id'=> 'required|numeric'
                    ];

                    break;
                }
                case 'PUT':
                {
                    return [
                        'descricao' => 'string|max:255',
                        'preco'=> 'numeric',
                        // 'imagem'=> '',
                        'frete_gratis'=> 'boolean',
                    ];

                    break;
                }
            }
    }
}
