<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'descricao' => 'required|string|max:255',
                    'store_id'=> 'required|numeric'
                ];

                break;
            }
            case 'PUT':
            {
                return [
                    'descricao' => 'string|max:255',
                ];

                break;
            }
        }
    }
}
