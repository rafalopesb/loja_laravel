<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            'name'     => 'Rafael Lopes',
            'username' => 'rafael_tray',
            'email'    => 'rafael.lopes@tray.net.br',
            'password' => Hash::make('123'),
        ));
    }
}
