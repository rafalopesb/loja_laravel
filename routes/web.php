<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard', array('uses' => 'WebControllers\DashboardController@index'));
Route::get('login', array('uses' => 'WebControllers\LoginController@index'));
Route::post('login', array('uses' => 'WebControllers\LoginController@login'));
Route::get('logout', array('uses' => 'WebControllers\LoginController@logout'));

Route::get('cadastros/produto', 'WebControllers\Cadastros\ProdutoController@index');
Route::get('cadastros/produto/novo', 'WebControllers\Cadastros\ProdutoController@novo');
Route::get('cadastros/produto/editar/{id}', 'WebControllers\Cadastros\ProdutoController@editar');
Route::post('cadastros/produto/salvar', 'WebControllers\Cadastros\ProdutoController@salvar');
Route::post('cadastros/produto/atualizar/{id}', 'WebControllers\Cadastros\ProdutoController@atualizar');
Route::get('cadastros/produto/deletar/{id}', 'WebControllers\Cadastros\ProdutoController@deletar');

Route::get('cadastros/loja', 'WebControllers\Cadastros\LojaController@index');
Route::get('cadastros/loja/novo', 'WebControllers\Cadastros\LojaController@novo');
Route::get('cadastros/loja/editar/{id}', 'WebControllers\Cadastros\LojaController@editar');
Route::post('cadastros/loja/salvar', 'WebControllers\Cadastros\LojaController@salvar');
Route::post('cadastros/loja/atualizar/{id}', 'WebControllers\Cadastros\LojaController@atualizar');
Route::get('cadastros/loja/deletar/{id}', 'WebControllers\Cadastros\LojaController@deletar');

Route::get('cadastros/categoria', 'WebControllers\Cadastros\CategoriaController@index');
Route::get('cadastros/categoria/novo', 'WebControllers\Cadastros\CategoriaController@novo');
Route::get('cadastros/categoria/editar/{id}', 'WebControllers\Cadastros\CategoriaController@editar');
Route::post('cadastros/categoria/salvar', 'WebControllers\Cadastros\CategoriaController@salvar');
Route::post('cadastros/categoria/atualizar/{id}', 'WebControllers\Cadastros\CategoriaController@atualizar');
Route::get('cadastros/categoria/deletar/{id}', 'WebControllers\Cadastros\CategoriaController@deletar');

Route::get('{storeid}', 'WebControllers\HomeController@index')->middleware('storeSlug');