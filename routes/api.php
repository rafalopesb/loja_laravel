<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'auth:api'
  ], function() {

        Route::get('/user', 'ApiControllers\AuthController@user');

        Route::resource('product', 'ApiControllers\ProdutoController');
        Route::resource('category', 'ApiControllers\CategoriaController');
        Route::resource('store', 'ApiControllers\LojaController');
  });

  Route::get('/search', 'ApiControllers\SearchController@index');
  Route::put('/search/index/{id}', 'ApiControllers\SearchController@createIndexStore');
  Route::post('/search/products', 'ApiControllers\SearchController@insertProductsInES');

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'ApiControllers\AuthController@login')->name('login');
    Route::post('signup', ['as'=> 'register', 'uses'=> 'ApiControllers\AuthController@signup']);
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'ApiControllers\AuthController@logout');
    });
});  